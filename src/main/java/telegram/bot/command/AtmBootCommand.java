package telegram.bot.command;

import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.bots.AbsSender;

import telegram.bot.util.MyBotCommand;
import telegram.bot.util.TelegramEmoji;
import telegram.bot.util.keyboard.TelegramBotBankKeyboard;

public class AtmBootCommand extends MyBotCommand{

	private TelegramBotBankKeyboard telegramBotBankKeyboard = new TelegramBotBankKeyboard();
	
	public AtmBootCommand(String commandIdentifier, String description) {
		super(commandIdentifier, description);
	}
	
	@Override
	public void execute(AbsSender absSender, Message message) {
		
		SendMessage sendMessage = new SendMessage();
		sendMessage.setChatId(message.getChatId().toString());
		sendMessage.setText("����������, ���������� �� ���� ����� ��������������� " + TelegramEmoji.EARTH_GLOBE_AMERICAS);
		sendMessage.setReplyMarkup(telegramBotBankKeyboard.getLocationKeyboard());
		
		try {
			absSender.sendMessage(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
		
	}
}
