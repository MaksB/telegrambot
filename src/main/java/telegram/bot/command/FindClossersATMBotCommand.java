package telegram.bot.command;

import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.api.methods.send.SendLocation;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Location;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.bots.AbsSender;

import telegram.bot.model.Device;
import telegram.bot.util.Find�loserObjectByLocation;
import telegram.bot.util.MyBotCommand;
import telegram.bot.util.TelegramEmoji;
import telegram.bot.util.keyboard.TelegramBotBankKeyboard;

public class FindClossersATMBotCommand extends MyBotCommand {

	private Find�loserObjectByLocation find�loserObjectByLocation = new Find�loserObjectByLocation();
	private TelegramBotBankKeyboard telegramBotBankKeyboard = new TelegramBotBankKeyboard();

	public FindClossersATMBotCommand(String commandIdentifier, String description) {
		super(commandIdentifier, description);
	}

	@Override
	public void execute(AbsSender absSender, Message message) {

		Location location = message.getLocation();
		Device device = find�loserObjectByLocation.findCloserAtm(location);

		SendMessage sendMessage = new SendMessage();
		sendMessage.setChatId(message.getChatId().toString());
		sendMessage.setText("��������� �������� " + device.getDistance().shortValue() + "� �� ���� \n"
				+ TelegramEmoji.WHITE_HEAVY_CHECK_MARK + device.getPlaceUa()+"\n"+TelegramEmoji.TRIANGULAR_FLAG_ON_POST +device.getFullAddressRu());
		sendMessage.setReplyMarkup(telegramBotBankKeyboard.getMainKeyboard());

		try {
			absSender.sendMessage(sendMessage);
		} catch (TelegramApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SendLocation sendLocation = new SendLocation();
		sendLocation.setChatId(message.getChatId().toString());
		sendLocation.setLatitude(Float.parseFloat(device.getLatitude()));
		sendLocation.setLongitude(Float.parseFloat(device.getLongitude()));

		try {
			absSender.sendLocation(sendLocation);
		} catch (TelegramApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
