package telegram.bot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Device {

	String type;
	String cityRU;
	String cityUA;
	String cityEN;
	String fullAddressRu;
	String fullAddressUa;
	String fullAddressEn;
	String placeRu;
	String placeUa;
	String latitude;
	String longitude;
	Double distance;
	
	
	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getCityRU() {
		return cityRU;
	}


	public void setCityRU(String cityRU) {
		this.cityRU = cityRU;
	}


	public String getCityUA() {
		return cityUA;
	}


	public void setCityUA(String cityUA) {
		this.cityUA = cityUA;
	}


	public String getCityEN() {
		return cityEN;
	}


	public void setCityEN(String cityEN) {
		this.cityEN = cityEN;
	}


	public String getFullAddressRu() {
		return fullAddressRu;
	}


	public void setFullAddressRu(String fullAddressRu) {
		this.fullAddressRu = fullAddressRu;
	}


	public String getFullAddressUa() {
		return fullAddressUa;
	}


	public void setFullAddressUa(String fullAddressUa) {
		this.fullAddressUa = fullAddressUa;
	}


	public String getFullAddressEn() {
		return fullAddressEn;
	}


	public void setFullAddressEn(String fullAddressEn) {
		this.fullAddressEn = fullAddressEn;
	}


	public String getPlaceRu() {
		return placeRu;
	}


	public void setPlaceRu(String placeRu) {
		this.placeRu = placeRu;
	}


	public String getPlaceUa() {
		return placeUa;
	}


	public void setPlaceUa(String placeUa) {
		this.placeUa = placeUa;
	}


	public String getLatitude() {
		return latitude;
	}


	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}


	public String getLongitude() {
		return longitude;
	}


	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	
	public Double getDistance() {
		return distance;
	}


	public void setDistance(Double distance) {
		this.distance = distance;
	}


	@Override
	public String toString() {
		return "Device [type=" + type + ", cityRU=" + cityRU + ", cityUA=" + cityUA + ", cityEN=" + cityEN
				+ ", fullAddressRu=" + fullAddressRu + ", fullAddressUa=" + fullAddressUa + ", fullAddressEn="
				+ fullAddressEn + ", placeRu=" + placeRu + ", placeUa=" + placeUa + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", distance=" + distance + "]";
	}

	
}
