package telegram.bot.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Valute {

	@XmlElement(name = "NumCode")
	private Integer numCode;
	@XmlElement(name = "CharCode")
	private String charCode;
	@XmlElement(name = "Nominal")
	private Integer nominal;
	@XmlElement(name = "Name")
	private String name;
	@XmlElement(name = "Value")
	private Float value;
	
	public Integer getNumCode() {
		return numCode;
	}
	public void setNumCode(Integer numCode) {
		this.numCode = numCode;
	}
	public String getCharCode() {
		return charCode;
	}
	public void setCharCode(String charCode) {
		this.charCode = charCode;
	}
	public Integer getNominal() {
		return nominal;
	}
	public void setNominal(Integer nominal) {
		this.nominal = nominal;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Float getValue() {
		return value;
	}
	public void setValue(Float value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "Valute [numCode=" + numCode + ", charCode=" + charCode + ", nominal=" + nominal + ", name=" + name
				+ ", value=" + value + "]";
	}
	
}
