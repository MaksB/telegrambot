package telegram.bot;

import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;

import telegram.bot.command.AtmBootCommand;
import telegram.bot.command.CurrencyExchangeCommand;
import telegram.bot.command.FindClossersATMBotCommand;
import telegram.bot.command.MyProfileBotCoommand;
import telegram.bot.command.StartBankBotCommad;
import telegram.bot.util.TelegramEmoji;
import telegram.bot.util.TelegramLongPollingCommandBot;
import telegram.bot.util.keyboard.TelegramBotBankKeyboard;

public class BankBot extends TelegramLongPollingCommandBot {

	private TelegramBotBankKeyboard telegramBotBankKeyboard = new TelegramBotBankKeyboard();

	public BankBot() {
		this.register(new StartBankBotCommad("start", "start working"));
		this.register(new MyProfileBotCoommand(TelegramEmoji.HOUSE_WITH_GARDEN + " mo� ������ �������", "������ �������"));
		this.register(new AtmBootCommand(TelegramEmoji.ATM + " ���������", "���������"));
		this.register(new FindClossersATMBotCommand("location", "���������"));
		this.register(new CurrencyExchangeCommand(TelegramEmoji.CURRENCY_EXCHANGE +" ���� �����", "���� �����"));
	}

	String pasword = "12345";
	String login = "";

	@Override
	public String getBotUsername() {
		return BotConfig.getBotName();
	}
	
	

	@Override
	public void processNonCommandUpdate(Update update) {
		System.out.println(update);


		if (update.hasCallbackQuery()) {
			CallbackQuery callbackQuery = update.getCallbackQuery();
			Message message = callbackQuery.getMessage();

			EditMessageText editMessageText = new EditMessageText();
			editMessageText.setChatId(message.getChatId().toString());
			if (callbackQuery.getData().equals("/Clear")) {
				if (message.getText().contains("*")) {
					editMessageText.setText(message.getText().replaceAll(".{2}$", "_"));
					editMessageText.setReplyMarkup(telegramBotBankKeyboard.getIlineAutentificationKeyboard());
					login = login.replaceAll(".$", "");
				} else {
					login = "";
					return;
				}
			} else if (callbackQuery.getData().equals("/Login")) {
				System.out.println(login);
				if (login.equals(pasword)) {
					editMessageText.setText("Welcome!");

				} else {
					editMessageText.setText("Invalide pasword!");
				}
				login = "";
			} else {
				editMessageText.setReplyMarkup(telegramBotBankKeyboard.getIlineAutentificationKeyboard());
				editMessageText.setText(message.getText().replaceAll(".$", "*_"));
				login = login + callbackQuery.getData();
			}
			editMessageText.setMessageId(message.getMessageId());
			editMessageText.enableHtml(true);

			try {
				editMessageText(editMessageText);
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}

		}

		

	}

	@Override
	public String getBotToken() {
		return BotConfig.getBotToken();
	}

}
