package telegram.bot.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

import org.telegram.telegrambots.api.objects.Location;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.AddressType;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

import telegram.bot.BotConfig;
import telegram.bot.model.Atm;
import telegram.bot.model.Device;

public class Find—loserObjectByLocation {
	
	private static final String GETATMPB_URL = "https://api.privatbank.ua/p24api/infrastructure?json&atm&city=";
	private String cityName;

	public Device findCloserAtm(Location curentlocation) {
		Device device = null;
		
		GeoApiContext context = new GeoApiContext().setApiKey(BotConfig.getApiKey());
		LatLng latLng = new LatLng(curentlocation.getLatitude(), curentlocation.getLongitude());

		try {
			GeocodingResult[] results = GeocodingApi.reverseGeocode(context, latLng).resultType(AddressType.LOCALITY)
					.await();
			cityName = results[0].addressComponents[0].longName;
			device = findCloserAtm(curentlocation, findAtmByCityAndStreet(cityName));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return device;
	}

	private Atm findAtmByCityAndStreet(String city) throws IOException {
		Atm atm;
		ObjectMapper mapper = new ObjectMapper();
		StringBuilder stringBuilder = new StringBuilder();
		
		URL url = new URL(GETATMPB_URL + city);

		try (BufferedInputStream bufferedInputStream = new BufferedInputStream(url.openStream())) {

			byte[] contents = new byte[1024];
			int bytesRead = 0;
			while ((bytesRead = bufferedInputStream.read(contents)) != -1) {
				stringBuilder.append(new String(contents, 0, bytesRead, "UTF-8"));
			}
		}

		atm = mapper.readValue(stringBuilder.toString(), Atm.class);

		return atm;

	}

	private Device findCloserAtm(Location currentLocation, Atm atm) {

		List<Device> listDevice = atm.getDevices().stream().peek(d -> {
			Double distance = distFrom(currentLocation.getLatitude(), currentLocation.getLongitude(),
					Double.parseDouble(d.getLatitude()), Double.parseDouble(d.getLongitude()));
			d.setDistance(distance);
		}).sorted((d1, d2) -> d1.getDistance().intValue() - d2.getDistance().intValue()).collect(Collectors.toList());
		
		Device device = listDevice.get(0);
		
		return device;
	}

	private Double distFrom(Double lat1, Double lng1, Double lat2, Double lng2) {
		double earthRadius = 6371000;
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		Double dist = earthRadius * c;

		return dist;
	}

}
