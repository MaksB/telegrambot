package telegram.bot.util;

import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.bots.commands.BotCommand;

public abstract class MyBotCommand extends BotCommand {

	public MyBotCommand(String commandIdentifier, String description) {
		super(commandIdentifier, description);
	
	}

	@Deprecated
	@Override
	public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
		// TODO Auto-generated method stub
		
	}
	
	 public abstract void execute(AbsSender absSender, Message message);
	
}
