package telegram.bot.util;

public enum TelegramEmoji {
	BANK("\uD83C\uDFE6"),
	ATM("\uD83C\uDFE7"),
	BLACK_TELEPHONE("\u260E"),
	HOUSE_WITH_GARDEN("\uD83C\uDFE1"),
	KEY("\uD83D\uDD11"),
	LOOK("\uD83D\uDD12"),
	ARROW_RIGHT("\u2935"),
	EARTH_GLOBE_AMERICAS("\uD83C\uDF0E"),
	WHITE_HEAVY_CHECK_MARK("\u2705"),
	TRIANGULAR_FLAG_ON_POST("\uD83D\uDEA9"),
	CURRENCY_EXCHANGE("\uD83D\uDCB1");
	
	private String unicode;
	
	TelegramEmoji(String unicode){
		this.unicode = unicode;
	}

	public String getEmoji() {
		return unicode;
	}
	
	@Override
	public String toString() {
		return getEmoji();
	}
	
}
