package telegram.bot.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.bots.commands.BotCommand;

public final class CommandRegistry implements ICommandRegistry {

    

	private final Map<String, MyBotCommand> commandRegistryMap = new HashMap<>();
    private BiConsumer<AbsSender, Message> defaultConsumer;

    @Override
    public void registerDefaultAction(BiConsumer<AbsSender, Message> defaultConsumer) {
        this.defaultConsumer = defaultConsumer;
    }

    @Override
    public final boolean register(MyBotCommand botCommand) {
        if (commandRegistryMap.containsKey(botCommand.getCommandIdentifier())) {
            return false;
        }
        commandRegistryMap.put(botCommand.getCommandIdentifier(), botCommand);
        return true;
    }

    @Override
    public final Map<MyBotCommand, Boolean> registerAll(MyBotCommand... botCommands) {
        Map<MyBotCommand, Boolean> resultMap = new HashMap<>(botCommands.length);
        for (MyBotCommand botCommand : botCommands) {
            resultMap.put(botCommand, register(botCommand));
        }
        return resultMap;
    }

    @Override
    public final boolean deregister(MyBotCommand botCommand) {
        if (commandRegistryMap.containsKey(botCommand.getCommandIdentifier())) {
            commandRegistryMap.remove(botCommand.getCommandIdentifier());
            return true;
        }
        return false;
    }

    @Override
    public final Map<MyBotCommand, Boolean> deregisterAll(MyBotCommand... botCommands) {
        Map<MyBotCommand, Boolean> resultMap = new HashMap<>(botCommands.length);
        for (MyBotCommand botCommand : botCommands) {
            resultMap.put(botCommand, deregister(botCommand));
        }
        return resultMap;
    }

    @Override
    public final Collection<MyBotCommand> getRegisteredCommands() {
        return commandRegistryMap.values();
    }

    @Override
    public final MyBotCommand getRegisteredCommand(String commandIdentifier) {
        return commandRegistryMap.get(commandIdentifier);
    }

    /**
     * Executes a command action if the command is registered.
     *
     * @note If the command is not registered and there is a default consumer,
     * that action will be performed
     *
     * @param absSender absSender
     * @param message input message
     * @return True if a command or default action is executed, false otherwise
     */
    public final boolean executeCommand(AbsSender absSender, Message message) {
        if (message.hasText()) {
            String text = message.getText().toLowerCase();            
            if(message.isCommand()){
            	text = text.substring(1);
            }

                if (commandRegistryMap.containsKey(text)) {
                    commandRegistryMap.get(text).execute(absSender, message);
                    return true;
                } else if (defaultConsumer != null) {
                    defaultConsumer.accept(absSender, message);
                    return true;
                }
            
        }else if(message.hasLocation()){
        	commandRegistryMap.get("location").execute(absSender, message);
            return true;
        }
        return false;
    }
    
    public Map<String, MyBotCommand> getCommandRegistryMap() {
		return commandRegistryMap;
	}
}
