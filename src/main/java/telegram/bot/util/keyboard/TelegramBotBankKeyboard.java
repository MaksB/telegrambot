package telegram.bot.util.keyboard;

import java.util.ArrayList;
import java.util.List;

import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import telegram.bot.util.TelegramEmoji;

public class TelegramBotBankKeyboard {

	public ReplyKeyboardMarkup getMainKeyboard() {
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setKeyboard(getMainKeyboardBoot());

		return replyKeyboardMarkup;

	}
	
	public ReplyKeyboardMarkup getLocationKeyboard(){
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setOneTimeKeyboad(true);
	
		
		List<KeyboardRow> rows = new ArrayList<KeyboardRow>();
		KeyboardRow row = new KeyboardRow();

		KeyboardButton keyboardButton = new KeyboardButton();
		keyboardButton.setText(TelegramEmoji.EARTH_GLOBE_AMERICAS + " Текущее местоположение");
		keyboardButton.setRequestLocation(true);
		row.add(keyboardButton);
		rows.add(row);
		
		row = new KeyboardRow();


		keyboardButton = new KeyboardButton();
		keyboardButton.setText(TelegramEmoji.ARROW_RIGHT + " Отмена");
		row.add(keyboardButton);
		
		rows.add(row);
		replyKeyboardMarkup.setKeyboard(rows);
		
		return replyKeyboardMarkup;
	}

	public InlineKeyboardMarkup getIlineAutentificationKeyboard() {
		List<List<InlineKeyboardButton>> l = new ArrayList<>();
		List<InlineKeyboardButton> l2 = new ArrayList<>();

		for (int i = 1; i < 10; i++) {
			
			InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
			inlineKeyboardButton.setText("" + i);
			inlineKeyboardButton.setCallbackData("" + i);
			l2.add(inlineKeyboardButton);
			if (i % 3 == 0) {
				l.add(l2);
				l2 = new ArrayList<>();
			}
		}

		InlineKeyboardButton inlineKeyboardButton10 = new InlineKeyboardButton();
		inlineKeyboardButton10.setText("0");
		inlineKeyboardButton10.setCallbackData("0");

		InlineKeyboardButton inlineKeyboardButtonExit = new InlineKeyboardButton();
		inlineKeyboardButtonExit.setText("Вход");
		inlineKeyboardButtonExit.setCallbackData("/Login");

		InlineKeyboardButton inlineKeyboardButtonClear = new InlineKeyboardButton();
		inlineKeyboardButtonClear.setText("Стереть");
		inlineKeyboardButtonClear.setCallbackData("/Clear");

		InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

		l2 = new ArrayList<>();

		l2.add(inlineKeyboardButtonClear);
		l2.add(inlineKeyboardButton10);
		l2.add(inlineKeyboardButtonExit);
		l.add(l2);

		inlineKeyboardMarkup.setKeyboard(l);
		return inlineKeyboardMarkup;
	}

	private List<KeyboardRow> getMainKeyboardBoot() {
		List<KeyboardRow> rows = new ArrayList<KeyboardRow>();

		KeyboardRow row = new KeyboardRow();

		KeyboardButton keyboardButton = new KeyboardButton();
		keyboardButton.setText(TelegramEmoji.CURRENCY_EXCHANGE + " Курс валют");
		row.add(keyboardButton);

		keyboardButton = new KeyboardButton();
		keyboardButton.setText(TelegramEmoji.ATM + " Банкоматы");
		row.add(keyboardButton);
		rows.add(row);

		row = new KeyboardRow();

		keyboardButton = new KeyboardButton();
		keyboardButton.setText(TelegramEmoji.HOUSE_WITH_GARDEN + " Moй личный кабинет");
		row.add(keyboardButton);
		

		keyboardButton = new KeyboardButton();
		keyboardButton.setText(TelegramEmoji.BLACK_TELEPHONE+ " Помощь");
		row.add(keyboardButton);
		rows.add(row);
		

		return rows;
	}

}
