package telegram.bot.util;

import java.util.Collection;
import java.util.Map;
import java.util.function.BiConsumer;

import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.bots.AbsSender;

public interface ICommandRegistry {
	 /**
     * Register a default action when there is no command register that match the message sent
     * @param defaultConsumer Consumer to evaluate the message
     *
     * @note Use this method if you want your bot to execute a default action when the user
     * sends a command that is not registered.
     */
    void registerDefaultAction(BiConsumer<AbsSender, Message> defaultConsumer);

    /**
     * register a command
     *
     * @param MyBotCommand the command to register
     * @return whether the command could be registered, was not already registered
     */
    boolean register(MyBotCommand myBotCommand);

    /**
     * register multiple commands
     *
     * @param MyBotCommands commands to register
     * @return map with results of the command register per command
     */
    Map<MyBotCommand, Boolean> registerAll(MyBotCommand... myBotCommands);

    /**
     * deregister a command
     *
     * @param MyBotCommand the command to deregister
     * @return whether the command could be deregistered, was registered
     */
    boolean deregister(MyBotCommand myBotCommand);

    /**
     * deregister multiple commands
     *
     * @param MyBotCommands commands to deregister
     * @return map with results of the command deregistered per command
     */
    Map<MyBotCommand, Boolean> deregisterAll(MyBotCommand... myBotCommands);

    /**
     * get a collection of all registered commands
     *
     * @return a collection of registered commands
     */
    Collection<MyBotCommand> getRegisteredCommands();

    /**
     * get registered command
     *
     * @return registered command if exists or null if not
     */
    MyBotCommand getRegisteredCommand(String commandIdentifier);
}
