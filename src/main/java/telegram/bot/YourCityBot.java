package telegram.bot;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.api.methods.AnswerInlineQuery;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.inlinequery.InlineQuery;
import org.telegram.telegrambots.api.objects.inlinequery.inputmessagecontent.InputTextMessageContent;
import org.telegram.telegrambots.api.objects.inlinequery.result.InlineQueryResult;
import org.telegram.telegrambots.api.objects.inlinequery.result.InlineQueryResultArticle;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;



public class YourCityBot extends TelegramLongPollingBot {

	public String getBotUsername() {
		return BotConfig.getBotName();
	}
	
	String pasword = "12345";
	String login = "";
	public void onUpdateReceived(Update update) {

		System.out.println(update);
		if(update.hasInlineQuery()){
			InlineQuery inlineQuery = update.getInlineQuery();
			InputTextMessageContent inputTextMessageContent = new InputTextMessageContent();
			inputTextMessageContent.setMessageText("dfffd");
			InlineQueryResultArticle inlineQueryResultArticle = new InlineQueryResultArticle();
			inlineQueryResultArticle.setId("4");
			inlineQueryResultArticle.setTitle("dfdf");
			inlineQueryResultArticle.setInputMessageContent(inputTextMessageContent);
			AnswerInlineQuery answerInlineQuery = new AnswerInlineQuery();
			answerInlineQuery.setCacheTime(145);
			answerInlineQuery.setInlineQueryId(inlineQuery.getId());
			answerInlineQuery.setResults(Arrays.asList(inlineQueryResultArticle));
			System.out.println(answerInlineQuery.toJson());
			try {
				answerInlineQuery(answerInlineQuery);
			} catch (TelegramApiException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		if(update.hasCallbackQuery()){
			CallbackQuery callbackQuery = update.getCallbackQuery();
			Message message  = callbackQuery.getMessage();
			
			System.out.println(message.getMessageId()+" "+message.getText());
			EditMessageText editMessageText = new EditMessageText();
			editMessageText.setChatId(message.getChatId().toString());
			if(callbackQuery.getData().equals("/Clear")){
				if(message.getText().contains("*")){
					editMessageText.setText(message.getText().replaceAll(".{2}$", "_"));
					editMessageText.setReplyMarkup(getIlineAutentificationKeyboard());
					login = login.replaceAll(".$", "");
				}else{
					login = "";
					return;
				}
			}else if(callbackQuery.getData().equals("/Login")){
				System.out.println(login);
				if(login.equals(pasword)){
					editMessageText.setText("Welcome!");
				
				}else{
					editMessageText.setText("Invalide pasword!");
				}
				login = "";
			}
			else{
				editMessageText.setReplyMarkup(getIlineAutentificationKeyboard());
				editMessageText.setText(message.getText().replaceAll(".$", "*_"));
				login = login + callbackQuery.getData();
			}
			editMessageText.setMessageId(message.getMessageId());
			editMessageText.enableHtml(true);
			try {
				editMessageText(editMessageText);
			} catch (TelegramApiException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		if (update.hasMessage()) {
			System.out.println(11);
			Message message = update.getMessage();
			
	
			if (message.hasText()) {
				InlineKeyboardMarkup inlineKeyboardMarkup = getIlineAutentificationKeyboard();

				/*
				 * ReplyKeyboardMarkup replyKeyboardMarkup = new
				 * ReplyKeyboardMarkup();
				 * 
				 * replyKeyboardMarkup.setSelective(true);
				 * replyKeyboardMarkup.setResizeKeyboard(true);
				 * replyKeyboardMarkup.setOneTimeKeyboad(true);
				 * replyKeyboardMarkup.setKeyboard(this.selectLanguage(message))
				 * ;
				 */

				SendMessage sendMessageRequest = new SendMessage();
				sendMessageRequest.setChatId(message.getChatId().toString());
				sendMessageRequest.setText("\uD83D\uDD12\uD83D\uDD11Введите пожалуйста ваш пароль\n_");
				sendMessageRequest.enableHtml(true);
				sendMessageRequest.setReplayMarkup(inlineKeyboardMarkup);
				
				
				

				
				try {

					sendMessage(sendMessageRequest);
				/*	
					 * try { Thread.sleep(5000); } catch (InterruptedException
					 * e) { // TODO Auto-generated catch block
					 * e.printStackTrace(); } editMessageText(editMessageText);
					 */
				} catch (TelegramApiException e) {
					e.printStackTrace();
				}
			}
		}

	}

	private InlineKeyboardMarkup getIlineAutentificationKeyboard() {
		List<List<InlineKeyboardButton>> l = new ArrayList<>();
		List<InlineKeyboardButton> l2 = new ArrayList<>();

		for (int i = 1; i < 10; i++) {
			
			InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
			inlineKeyboardButton.setText("" + i);
			inlineKeyboardButton.setCallbackData("" + i);
			l2.add(inlineKeyboardButton);
			if (i % 3 == 0) {
				l.add(l2);
				l2 = new ArrayList<>();
			}
		}

		InlineKeyboardButton inlineKeyboardButton10 = new InlineKeyboardButton();
		inlineKeyboardButton10.setText("0");
		inlineKeyboardButton10.setCallbackData("0");

		InlineKeyboardButton inlineKeyboardButtonExit = new InlineKeyboardButton();
		inlineKeyboardButtonExit.setText("Вход");
		inlineKeyboardButtonExit.setCallbackData("/Login");

		InlineKeyboardButton inlineKeyboardButtonClear = new InlineKeyboardButton();
		inlineKeyboardButtonClear.setText("Стереть");
		inlineKeyboardButtonClear.setCallbackData("/Clear");

		InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

		l2 = new ArrayList<>();

		l2.add(inlineKeyboardButtonClear);
		l2.add(inlineKeyboardButton10);
		l2.add(inlineKeyboardButtonExit);
		l.add(l2);

		inlineKeyboardMarkup.setKeyboard(l);
		return inlineKeyboardMarkup;
	}

	/*
	 * public List<KeyboardRow> selectLanguage(Message message){ changed fom
	 * "List<List<String>>" to "List<KeyboardRow>" Now we have just a one
	 * dimension array. Like a stack or something like that
	 * 
	 * List<KeyboardRow> rows = new ArrayList<KeyboardRow>();
	 * 
	 * 
	 * //Instead of a list that collects our strings, or "buttons" for each row,
	 * now we have a own Object for that KeyboardRow row = new KeyboardRow();
	 * 
	 * rows.add(row);
	 * 
	 * row = new KeyboardRow(); row.add("🇦🇹 Deutsch (Östereich)");
	 * rows.add(row);
	 * 
	 * //I just reused the object above. Of cource you could also create a new
	 * Keyboardrow for each real row row = new KeyboardRow();
	 * row.add("🇩🇪 Deutsch (Deutschland)"); rows.add(row);
	 * 
	 * row = new KeyboardRow(); row.add("🇧🇷 Português"); rows.add(row);
	 * 
	 * row = new KeyboardRow(); row.add("🇺🇸 English (United States)");
	 * rows.add(row);
	 * 
	 * return rows; }
	 */
	@Override
	public String getBotToken() {
		return BotConfig.getBotToken();
	}

}
